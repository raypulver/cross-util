"use strict";

const gulp = require('gulp'),
  path = require('path'),
  fs = require('fs'),
  _ = require('lodash'),
  webpack = require('webpack'),
  gutil = require('gulp-util'),
  runSequence = require('run-sequence'),
  mocha = require('gulp-mocha'),
  babel = require('gulp-babel'),
  ClosureCompilerPlugin = require('webpack-closure-compiler'),
  jshint = require('gulp-jshint'),
  spawnSync = require('child_process').spawnSync,
  clone = require('clone'),
  map = require('async').map,
  Karma = require('karma').Server,
  webpackConfig = require('./webpack.config');

let jshintConfig = {};

require.extensions['.json'](jshintConfig, './.jshintrc');

let jshintServerConfig = jshintConfig.exports;
let jshintClientConfig = clone(jshintServerConfig);

jshintClientConfig.predef = jshintClientConfig.predef.client;
jshintServerConfig.predef = jshintServerConfig.predef.server;

let packageJson = require('./package');

gulp.task('default', ['build']);

const assetPath = 'assets/**/*';

const srcPath = 'src/**/*.js';

const reserved = ['default'];

jshint.client = jshint.bind(null, jshintClientConfig);
jshint.server = jshint.bind(null, jshintServerConfig);

gulp.task('build:tasks', function () {
  packageJson.scripts = {};
  _.forOwn(gulp.tasks, function (value, key, obj) {
    if (~reserved.indexOf(key)) return;
    packageJson.scripts[key] = `node ${path.join('node_modules', 'gulp', 'bin', 'gulp')} ${key}`;
  });
  fs.writeFileSync('./package.json', JSON.stringify(packageJson, null, 1));
});

gulp.task('test', function () {
  spawnSync('karma', ['start', 'karma.conf.js'], { stdio: 'inherit' });
});

gulp.task('build', function (cb) {
  runSequence(['build:private', 'jshint', 'test'], cb);
});

gulp.task('watch', ['default'], function () {
  return gulp.watch([ assetPath, srcPath ], ['default']);
});

gulp.task('build:private', ['build:server', 'build:client']);

gulp.task('build:server', function () {
  gulp.src('src/**/*.js')
    .pipe(babel({ presets: ['es2015'] }))
    .pipe(gulp.dest('dist'));
});

function copyIndex() {
  return gulp.src('./assets/index*.html')
    .pipe(gulp.dest('./public/'));
}
function copyImages() {
  return gulp.src('./assets/images/**/*')
    .pipe(gulp.dest('./public/images'));
}
function copyFavicon() {
  return gulp.src('./assets/favicon.*')
    .pipe(gulp.dest('./public/'));
}

gulp.task('build:client', ['webpack', 'copy']);

gulp.task('copy:index', copyIndex);

gulp.task('copy:favicon', copyFavicon);
gulp.task('copy:images', copyImages);

gulp.task('copy', ['copy:index', 'copy:images', 'copy:favicon']);

gulp.task('build:client:dev', ['webpack:dev'], copyIndex);

function doWebpack(next) {
  webpack(webpackConfig, function (err, stats) {
    if (err) throw new gutil.PluginError('webpack', err);
    gutil.log('[webpack]', stats.toString({
      colors: true,
      chunkModules: false
    }));
    next();
  });
}

function doWebpackUnmin(next) {
  webpackConfig.plugins.splice(webpackConfig.plugins.findIndex((v) => {
    return v instanceof ClosureCompilerPlugin;
  }), 1);
  doWebpack(next);
}

gulp.task('webpack', doWebpack);

gulp.task('webpack:dev', doWebpackUnmin);

gulp.task('jshint', ['jshint:server', 'jshint:client']);


gulp.task('jshint:server', function () {
  return gulp.src('./src/**/*.js')
    .pipe(jshint.server())
    .pipe(jshint.reporter('jshint-stylish'));
});
gulp.task('jshint:client', function () {
  return gulp.src('./assets/**/*.js')
    .pipe(jshint.client())
    .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('build:abi', function () {
  spawnSync('node', [path.join(__dirname, 'contracts', 'build-json-abi'), 'all'], { stdio: 'inherit' });
});
