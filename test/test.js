describe('cross-util utility library', function () {
  it('should do create natively', function () {
    var created;
    expect((created = cross.create({ a: 5 }, { b: 6 }))).to.have.keys(['b']);
    expect(Object.getPrototypeOf(created)).to.have.keys(['a']);
  });
  it('should shim create', function () {
    var created;
    expect((created = cross._createShim({ a: 5 }, { b: 6 }))).to.have.keys(['b']);
    expect(Object.getPrototypeOf(created)).to.have.keys(['a']);
  });
  it('should shim Array#map', function () {
    expect(cross._mapShim.call([1, 5, 9], function (v) {
      return v*3;
    })).to.eql([3, 15, 27]);
  });
  it('should shim Array#find', function () {
    expect(cross.find([1, 5, 9], function (v) {
      return !(v % 3);
    })).to.equal(9);
    expect(cross.find([1, 5, 9], function (v, i) {
      return i === 3;
    })).to.be.null;
  });
  it('should shim Array#forEach', function () {
    var results = [], data;
    cross._forEachShim.call((data = [1, 5, 9]), function (v, i) {
      results.push(i);
    });
    expect(results).to.eql([0, 1, 2]);
  });
});
