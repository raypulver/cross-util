"use strict";

import { curry, uncurryThis, compose } from 'funktional';

const {
  hasOwnProperty,
  toString
} = Object.prototype;

const { getOwnPropertyDescriptor } = Object;

function keysShim(o) {
  let retval = [];
  for (let i in o) {
    if (hasOwnProperty.call(o, i)) {
      retval.push(i);
    }
  }
  return retval;
}

const keys = Object.keys || keysShim;

const forEach = Array.prototype.forEach || forEachShim;

function forEachShim(cb, thisArg) {
  for (let i = 0; i < this.length; ++i) {
    cb.call(thisArg, this[i], i, this);
  }
}

function mapShim (cb, thisArg) {
  let retval = [];
  forEach.call(this, function (v, i, arr) {
    retval.push(cb.call(thisArg, v, i, arr));
  });
  return retval;
}

function assignShim(dest, src) {
  forEach.call(keys(src), function (v) {
    dest[v] = src[v];
  });
  return dest;
}

let assign = Object.assign || assignShim;

function forOwn(obj, cb, thisArg) {
  return forEachEarlyExit.call(keys(obj), function (v, i) {
    return cb.call(thisArg, obj[v], v, i, obj);
  });
}

function mapOwn(obj, cb, thisArg) {
  var retval = {};
  forOwn(obj, function (value, key) {
    retval[key] = cb.apply(thisArg, arguments);
  });
  return retval;
}

function createNative(proto, o) {
  return Object.create(proto, mapOwn(o, function (value, key, i, obj) {
    return getOwnPropertyDescriptor(obj, key);
  }));
}

function createShim (proto, o) {
  function Type() {}
  Type.prototype = proto;
  let retval = new Type();
  return assign(retval, o);
}

const create = Object.create && Object.getOwnPropertyDescriptor && createNative || createShim;

function forEachEarlyExit(fn, thisArg) {
  try {
    forEach.call(this, function (v, i, arr) {
      if (fn.call(thisArg, v, i, arr) === false) throw Error('@EarlyExit');
    });
    return true;
  } catch (e) {
    if (e.message === '@EarlyExit') return false;
    throw e;
  }
}

function isDate (d) {
  return toString.call(d) === '[object Date]';
}

function isArrayShim(a) {
  return toString.call(d) === '[object Array]';
}

const isArray = Array.isArray || isArrayShim;

function simpleEqual(a, b, depth) {
  if (typeof depth === 'undefined') depth = 1;
  if (!depth) return true;
  switch (typeof a) {
    case 'string':
    case 'boolean':
    case 'undefined':
    case 'function':
      return a === b;
    case 'number':
      // handle NaN
      if (a !== a) return b !== b;
      return a === b;
    case 'function':
    case 'object':
      if (typeof b !== 'object') return false;
      if (isArray(a)) {
        if (!isArray(b)) return false;
        if (a.length !== b.length) return false;
        if (!forEachEarlyExit.call(a, function (v, i) {
          if (!simpleEqual(v, b[i], depth - 1)) return false;
        })) return false;
        return true;
      }
      if (isDate(a)) {
        if (!isDate(b)) return false;
        else return a.getTime() === b.getTime();
      }
      if (!forOwn(a, function (value, key) {
        if (!simpleEqual(value, b[key], depth - 1)) return false;
      })) {
        return false;
      }
      return true;
  }
}

const find = Array.prototype.find || function (fn, thisArg) {
  let result = null;
  forEachEarlyExit.call(this, function (v, i, arr) {
    if (fn.call(thisArg, v, i, arr)) {
      result = v;
      return false;
    }
  });
  return result;
};

const findIndex = Array.prototype.findIndex || function (fn, thisArg) {
  let idx = -1;
  forEachEarlyExit.call(this, function (v, i, arr) {
    if (fn.call(thisArg, v, i, arr)) {
      idx = i;
      return false;
    }
  });
  return idx;
};

const map = Array.prototype.map || mapShim;

module.exports = create(null, {
  keys,
  _keysShim: keysShim,
  create,
  _createShim: createShim,
  assign: assign,
  _assignShim: assignShim,
  mapOwn: mapOwn,
  forOwn: forOwn,
  forEach: uncurryThis(forEachEarlyExit),
  _forEachShim: forEachShim,
  map: uncurryThis(map),
  _mapShim: mapShim,
  equal: simpleEqual,
  find: uncurryThis(find),
  findIndex: uncurryThis(findIndex)
});
