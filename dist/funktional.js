"use strict";

function each(arr, fn, thisArg) {
  for (var i = 0; i < arr.length; ++i) {
    fn.call(thisArg, arr[i], i, arr);
  }
  return true;
}

function reduceShim(arr, fn, initial, thisArg) {
  forEach(arr, function (v, i, arr) {
    initial = fn.call(thisArg, initial, v, i, arr);
  });
  return initial;
}

var forEach = [].forEach && uncurryThis([].forEach) || each;

var reduce = [].reduce && uncurryThis([].reduce) || reduceShim;

function curry(fn) {
  var args = [].slice.call(arguments, 1);
  return function () {
    return fn.apply(this, args.concat([].slice.call(arguments)));
  };
}

function curryThis(fn, thisArg) {
  return function () {
    return fn.apply(thisArg, arguments);
  };
}

function uncurryThis(fn) {
  return function () {
    var self = arguments[0];
    return fn.apply(self, [].slice.call(arguments, 1));
  };
}

function compose() {
  var fns = [].slice.call(arguments);
  return function (arg) {
    var self = this;
    return reduce(fns, function (r, v) {
      return v.call(self, r);
    }, arg);
  };
}

module.exports = {
  curry: curry,
  curryThis: curryThis,
  uncurryThis: uncurryThis,
  compose: compose
};