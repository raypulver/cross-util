"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

var _funktional = require('funktional');

var _Object$prototype = Object.prototype;
var hasOwnProperty = _Object$prototype.hasOwnProperty;
var toString = _Object$prototype.toString;
var getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;


function keysShim(o) {
  var retval = [];
  for (var i in o) {
    if (hasOwnProperty.call(o, i)) {
      retval.push(i);
    }
  }
  return retval;
}

var keys = Object.keys || keysShim;

var forEach = Array.prototype.forEach || forEachShim;

function forEachShim(cb, thisArg) {
  for (var i = 0; i < this.length; ++i) {
    cb.call(thisArg, this[i], i, this);
  }
}

function mapShim(cb, thisArg) {
  var retval = [];
  forEach.call(this, function (v, i, arr) {
    retval.push(cb.call(thisArg, v, i, arr));
  });
  return retval;
}

function assignShim(dest, src) {
  forEach.call(keys(src), function (v) {
    dest[v] = src[v];
  });
  return dest;
}

var assign = Object.assign || assignShim;

function forOwn(obj, cb, thisArg) {
  return forEachEarlyExit.call(keys(obj), function (v, i) {
    return cb.call(thisArg, obj[v], v, i, obj);
  });
}

function mapOwn(obj, cb, thisArg) {
  var retval = {};
  forOwn(obj, function (value, key) {
    retval[key] = cb.apply(thisArg, arguments);
  });
  return retval;
}

function createNative(proto, o) {
  return Object.create(proto, mapOwn(o, function (value, key, i, obj) {
    return getOwnPropertyDescriptor(obj, key);
  }));
}

function createShim(proto, o) {
  function Type() {}
  Type.prototype = proto;
  var retval = new Type();
  return assign(retval, o);
}

var create = Object.create && Object.getOwnPropertyDescriptor && createNative || createShim;

function forEachEarlyExit(fn, thisArg) {
  try {
    forEach.call(this, function (v, i, arr) {
      if (fn.call(thisArg, v, i, arr) === false) throw Error('@EarlyExit');
    });
    return true;
  } catch (e) {
    if (e.message === '@EarlyExit') return false;
    throw e;
  }
}

function isDate(d) {
  return toString.call(d) === '[object Date]';
}

function isArrayShim(a) {
  return toString.call(d) === '[object Array]';
}

var isArray = Array.isArray || isArrayShim;

function simpleEqual(a, b, depth) {
  if (typeof depth === 'undefined') depth = 1;
  if (!depth) return true;
  switch (typeof a === 'undefined' ? 'undefined' : _typeof(a)) {
    case 'string':
    case 'boolean':
    case 'undefined':
    case 'function':
      return a === b;
    case 'number':
      // handle NaN
      if (a !== a) return b !== b;
      return a === b;
    case 'function':
    case 'object':
      if ((typeof b === 'undefined' ? 'undefined' : _typeof(b)) !== 'object') return false;
      if (isArray(a)) {
        if (!isArray(b)) return false;
        if (a.length !== b.length) return false;
        if (!forEachEarlyExit.call(a, function (v, i) {
          if (!simpleEqual(v, b[i], depth - 1)) return false;
        })) return false;
        return true;
      }
      if (isDate(a)) {
        if (!isDate(b)) return false;else return a.getTime() === b.getTime();
      }
      if (!forOwn(a, function (value, key) {
        if (!simpleEqual(value, b[key], depth - 1)) return false;
      })) {
        return false;
      }
      return true;
  }
}

var find = Array.prototype.find || function (fn, thisArg) {
  var result = null;
  forEachEarlyExit.call(this, function (v, i, arr) {
    if (fn.call(thisArg, v, i, arr)) {
      result = v;
      return false;
    }
  });
  return result;
};

var findIndex = Array.prototype.findIndex || function (fn, thisArg) {
  var idx = -1;
  forEachEarlyExit.call(this, function (v, i, arr) {
    if (fn.call(thisArg, v, i, arr)) {
      idx = i;
      return false;
    }
  });
  return idx;
};

var map = Array.prototype.map || mapShim;

module.exports = create(null, {
  keys: keys,
  _keysShim: keysShim,
  create: create,
  _createShim: createShim,
  assign: assign,
  _assignShim: assignShim,
  mapOwn: mapOwn,
  forOwn: forOwn,
  forEach: (0, _funktional.uncurryThis)(forEachEarlyExit),
  _forEachShim: forEachShim,
  map: (0, _funktional.uncurryThis)(map),
  _mapShim: mapShim,
  equal: simpleEqual,
  find: (0, _funktional.uncurryThis)(find),
  findIndex: (0, _funktional.uncurryThis)(findIndex)
});