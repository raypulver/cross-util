"use strict";

const path = require('path'),
  webpack = require('webpack'),
  ClosureCompilerPlugin = require('webpack-closure-compiler'),
  assetPath = path.join(__dirname, 'src'),
  publicPath = path.join(__dirname);

module.exports = {
  entry: path.join(assetPath, 'cross-util.js'),
  output: {
    path: publicPath,
    filename: 'cross-util.js',
    library: 'cross',
    libraryTarget: 'umd'
  },
  devtool: 'source-map',
  plugins: [
    new ClosureCompilerPlugin({
      compiler: {
        language_in: 'ECMASCRIPT6',
        language_out: 'ECMASCRIPT5'
      },
      concurrency: 3
    })
  ],
  module: {
    loaders: [{
      test: /(?:\.js$)/,
      loader: 'babel-loader',
      exclude: /node_modules/,
      query: {
        presets: ['es2015'],
        plugins: ['transform-es3-member-expression-literals', 'transform-es3-property-literals']
      }
    }]
  }
};
